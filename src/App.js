import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="app">
        <p className="app-intro">
          Microsite for new feature of DiDiDi
        </p>
      </div>
    );
  }
}

export default App;
